<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Factory as Faker;
use App\Models\User;
use App\Models\Site;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $faker = Faker::create();
        for ($i=1; $i <= 10; $i++) {
            if($i==1){
                $user = User::updateOrCreate(['id' => $i],
                    [
                        'name'           => 'Admin',
                        'email'          => 'admin@yopmail.com',
                        'password'       => bcrypt('123456'),
                        'role_id'      => 1,
                    ]
                );

                $site = Site::updateOrCreate([
                    'user_id' => $user->id
                ]);

                $user->site_id = $site->id;
                $user->save();
            }else{
                $user = User::updateOrCreate(['id' => $i],
                    [
                        'name'      => $faker->name,
                        'email'     => $faker->email,
                        'password'  => bcrypt('123456'),
                        'role_id'   => rand(1, 2),
                        'site_id'   => $site->id
                    ]
                );
            }
            
        }
    }
}
