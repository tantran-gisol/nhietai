@extends('auth.layouts.app')

@section('content')
<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="admin-login-input col-12">
        <input type="email" placeholder="{{ __('Địa chỉ email') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required />
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="admin-login-pass col-12">
        <input type="password" placeholder="{{ __('Mật khẩu') }}" class="form-control @error('password') is-invalid @enderror" name="password" required />
        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
    <div class="col-12 text-center">
        <button class="btn btn-no-radius admin-login-btn w-100">{{ __('Đăng nhập') }}</button>
    </div>
</form>
@if (Route::has('password.request'))
    <div class="col-12 text-center">
        <label class="forget-txt">
            <a href="{{ route('password.request') }}">{{ __('Nếu bạn quên mật khẩu của mình, hãy nhấp vào đây') }}</a>
        </label>
    </div>
@endif
@endsection