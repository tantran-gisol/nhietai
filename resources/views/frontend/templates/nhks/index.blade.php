@extends('frontend.layouts.app')
@section('content')
  <main id="main">
    <section id="about">
      <div class="container">
        <h5>BÀI VIẾT MỚI</h5>
        <ul class="list-group" style="float: left;width: 25%;font-size:15px">
          <li class="list-group-item">Du học Nhật Bản ngành xây dựng</li>
          <li class="list-group-item">Du học Nhật Bản ngành Đầu bếp</li>
          <li class="list-group-item">Du học Nhật Bản ngành cơ khí, ngành kỹ thuật ô tô</li>
          <li class="list-group-item">Du học Nhật Bản ngành du lịch nhà hàng khách sạn</li>
          <li class="list-group-item">Du học Nhật Bản ngành công nghệ thông tin năm</li>
        </ul>

        <div class="row about-cols" style="margin-left: 30%">
          <h3 class="section-header">Du học Nhật Bản ngành công nghệ thông tin năm 2018 – 2019</h3>            
          <img src="{{ asset('image/nh-ks.jpg') }}" alt="" class="img-fluid">
          <!-- <div class="icon"><i class="ion-ios-speedometer-outline"></i></div> -->
          <!-- <h2 class="title"><a href="#">Danh sách câu hỏi phỏng vấn của Cục xuất nhập cảnh Nhật Bản</a></h2> -->
          <p style="font-weight: bold;font-size:16px"> 
            1. Tại sao nên du học Nhật Bản ngành du lich nhà hàng khách sạn?
          </p>
          <p style="font-size:14px">
            Chúng ta đều biết, Nhật Bản là một trong những quốc gia có nền giáo dục tiên tiến hàng đầu trên thế giới. Chương trình đào tạo tại Nhật không chỉ chú trọng tới việc cung cấp kiến thức cho sinh viên mà muốn đào tạo lực lượng lao động có trình độ chuyên môn và kỹ năng thực chất cho xã hội. Lựa chọn du học Nhật Bản ngành du lịch nhà hàng khách sạn không chỉ cho các bạn cơ hội tiếp xúc và học tập trong nền giáo dục tiên tiến mà còn là cơ hội giúp các bạn được rèn luyện bản thân, học hỏi tính chỉn chu và nguyên tắc làm việc chuyên nghiệp của người Nhật.<br><br>
            Không chỉ nổi tiếng về giáo dục, xứ sở hoa anh đào còn là điểm đến thu hút khách du lịch đông nhất trên thế giới. Du khách tới đây sẽ được chiêm ngưỡng những kiến trúc tuyệt đẹp, những nét văn hóa dân tộc vẫn được gìn giữ và phát huy một cách tích cực. Với môi trường du lịch phát triển như vậy, sinh viên ngành du lịch tại Nhật Bản sẽ nhanh chóng nắm bắt và xây dựng cho bản thân những kỹ năng công việc cần thiết. Quan trọng hơn các bạn sinh viên quốc tế du học ngành du lịch tại Nhật Bản sẽ học hỏi được những điều tuyệt vời trong việc gìn giữ giá trị truyền thống, cách phát huy và thúc đẩy văn hóa dân tộc tới với bạn bè thế giới, phương pháp kinh doanh du lịch thông minh của người Nhật,… Có rất nhiều điều thú vị đang chờ đón các bạn.<br><br>
            Đặc biệt, các bạn sinh viên khi du học Nhật Bản ngành du lịch nhà hàng khách sạn sẽ có cơ hội được làm thêm trong thời gian học tập. Đây chính là cơ hội tuyệt vời để các bạn thực hành và học hỏi những kinh nghiệm thực tế từ các anh chị đi trước. Những công việc làm thêm tuy không mang lại nguồn thu nhập quá lớn nhưng là cơ hội tuyệt vời mà không phải ai cũng có được, nhất là với sinh viên học ngành du lịch. Các bạn có thể làm hướng dẫn viên cho khách nước ngoài tới Nhật Bản (trong đó có những đoàn du lịch từ Việt Nam sang Nhật) hoặc cho chính khách Nhật Bản đi tham quan trong nước. Các bạn sẽ vừa được khám phá nước Nhật miễn phí, vừa được học hỏi kinh nghiệm, nâng cao khả năng ngoại ngữ và làm giàu kiến thức cho bản thân.<br><br>
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            2. Du học Nhật Bản ngành du lịch nhà hàng khách sạn có những chuyên ngành gì<br>
          </p>
          <p style="font-size:14px">
            Khi lựa chọn du học Nhật Bản ngành du lịch nhà hàng khách sạn bạn không chỉ được đào tạo trong khuôn khổ của chuyên ngành này mà còn có cơ hội học tập và nghiên cứu nhiều lĩnh vực liên quan, hấp dẫn khác. Các lĩnh vực đào tạo
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            3. Câu hỏi về mục đích du học Nhật Bản.<br>
          </p>
          <p style="font-size:14px">
           Để có thể đăng ký đi du học Nhật Bản ngành du lịch cũng như những ngành học khác, các bạn cần chuẩn bị những yêu cầu cơ bản<br><br>
           Các bạn học sinh cũng không cần quá áp lực về kì thi đại học này, bởi các trường Nhật ngữ ở Nhật cũng đều có những khóa ôn luyện thi EJU (hay còn gọi là khóa dự bị đại học) giúp các bạn chuẩn bị vững vàng cho kì thi sau khi đã có đủ trình độ tiếng Nhật.<br><br>
            Hiện tại ngành Du lịch khách sạn vẫn đang thiếu nguồn nhân lực một cách trầm trọng, đặc biệt nguồn nhân lực được đào tạo trong nước không đáp ứng được các tiêu chuẩn quốc tế. Vì vậy, việc du học Nhật Bản ngành du lịch nhà hàng khách sạn sẽ giúp bạn có rất nhiều lợi thế trong nghề nghiệp. Để biết thêm thông tin về khóa học và trường lớp, nhanh tay đăng ký tư vấn tại đây bạn nhé!<br><br>
          </p>                    
        </div>
        <div class="form">
          <div id="sendmessage" style="font-weight:bold;">Email của bạn sẽ không được hiển thị công khai. Các trường bắt buộc được đánh dấu *</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><input type="submit"  value="Subscribe" style="background: #28a745;border: 0;width: 35%;padding: 6px 0;text-align: center;color: #fff;transition: 0.3s;cursor: pointer;"></div>
          </form>
        </div>
      </div>
    </section>
  </main>
@endsection