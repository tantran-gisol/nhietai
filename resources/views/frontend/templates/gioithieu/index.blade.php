@extends('frontend.layouts.app')
@section('content')
  <section id="intro" style="height:42vh">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner" role="listbox">

          <div class="carousel-item active" style="height:400px;">
            <div class="carousel-background">
              <img src="{{ asset('image/lienhe.png') }}">
            </div>
            <div class="carousel-container">
              <div class="carousel-content">
                <h2>GIỚI THIỆU</h2>
                <!-- <p>Thành công của tương lai bắt đầu từ hôm nay.</p> -->
                <!-- <a href="#featured-services" class="btn-get-started scrollto">Get Started</a> -->
              </div>
            </div>
          </div>

        </div>

      </div>
    </div>
  </section><!-- #intro -->

  <main id="main">
    <section id="services">
      <div class="container">
        <h3 class="section-header">Trường Đại học quốc tế Mona Media</h3>
        <div class="row" style="width: 160%;">
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <h4 class="title" style="text-align:left;color:#0071bb;font-size: 15px;">Thông tin về trường</h4>
            <p class="description" style="text-align:left;font-size: 13px;">Trường đại học Mona Media là trườ đại học quốc tế hàng đầu, Mona Media thành lập từ năm 2001 đến nay Mona Media đã có những bước tiến vững chắc trên thị trường. Mỗi năm, thông qua việc tuyển sinh du học Nhật Bản có hàng trăm học sinh – sinh viên Việt Nam được Mona Media kết nối và hỗ trợ thực hiện các chuyến du học Nhật Bản.<br><br>
            Với mong muốn chắp cánh cho những ước mơ, đào tạo những tài năng Việt đến với nền tri thức mới tại Nhật Bản, đội ngũ nhân sự công ty sẽ tư vấn nhiệt tình và cung cấp những thông tin đáng tin cậy nhất đến các bạn.<br><br>
            Du học Mona Media với đội ngũ cán bộ nhân viên hồ sơ và tư vấn là những con người Việt Nam đã từng làm việc, du học sinh, thực tập sinh tại Nhật Bản. Việc làm hồ sơ du học Nhật đòi hỏi tính chính xác tuyệt đối. Người Nhật nổi tiếng với sự trung thực, cần cù, tỉ mỉ và chính xác. Chính vì vậy, bạn hoàn toàn yên tâm khi giao của bạn cho các chuyên viên du học của Nhật Anh, với kinh nghiệm hơn 10 năm về lao động nước ngoài và 5 năm về du học, bạn hoàn toàn yên tâm khi làm hồ sơ tại du học Mona Media.
            </p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="img">
              <img src="http://mauweb.monamedia.net/blueuni/wp-content/uploads/2019/01/pic_gt.jpeg" class="attachment-original size-original">
            </div>
          </div>
        </div>

      </div>
    </section>

    <section id="facts"  class="wow fadeIn">
      <div class="container">
        <div class="row counters">

          <div class="col-lg-3 col-6 text-center">
            <img src="http://mauweb.monamedia.net/blueuni/wp-content/uploads/2019/01/book.png" class="attachment-original size-original" alt="">
            <span data-toggle="counter-up">168</span>
            <p>HỒ SƠ ĐĂNG KÝ</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <img src="http://mauweb.monamedia.net/blueuni/wp-content/uploads/2019/01/female-graduate-student.png" class="attachment-original size-original" alt="">
            <span data-toggle="counter-up">3500</span>
            <p>SINH VIÊN THEO HỌC</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <img src="http://mauweb.monamedia.net/blueuni/wp-content/uploads/2019/01/graduation-hat-and-diploma-1.png" class="attachment-original size-original" alt="">
            <span data-toggle="counter-up">350</span>
            <p>CHỨNG NHẬN BẰNG CẤP</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <img src="http://mauweb.monamedia.net/blueuni/wp-content/uploads/2019/01/multiple-users-silhouette.png" class="attachment-original size-original" alt="">
            <span data-toggle="counter-up">20</span>
            <p>ĐỘI NGŨ GIẢNG VIÊN</p>
          </div>

        </div>

      </div>
    </section>

    <section id="services">
      <div class="container">

        <div class="row" style="width: 160%;">

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon">
              <img src="http://mauweb.monamedia.net/blueuni/wp-content/uploads/2019/01/money.png" class="attachment-medium size-medium" alt="">
            </div>
            <h4 class="title"><a href="">CHI PHÍ HỢP LÝ</a></h4>
            <p class="description">Nhận thức rõ rằng việc đi là cả một sự đầu tư lớn đối với phụ huynh và học sinh. Phụ huynh đã phải đầu tư nhiều chi phí và đặt bao niềm tin và hy vọng vào các bạn trẻ. Còn với các bạn, đó là bước ngoặt quan trọng của tương lai, của cuộc đời. Do vậy, chúng tôi chỉ giới thiệu với các bạn danh sách trường Nhật tốt nhất mà thôi.</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon">
              <img src="http://mauweb.monamedia.net/blueuni/wp-content/uploads/2019/01/badge.png" class="attachment-medium size-medium" alt="">
            </div>
            <h4 class="title"><a href="">CHẤT LƯỢNG ĐÀO TẠO UY TÍN</a></h4>
            <p class="description">Nhận thức rõ rằng việc đi là cả một sự đầu tư lớn đối với phụ huynh và học sinh. Phụ huynh đã phải đầu tư nhiều chi phí và đặt bao niềm tin và hy vọng vào các bạn trẻ. Còn với các bạn, đó là bước ngoặt quan trọng của tương lai, của cuộc đời. Do vậy, chúng tôi chỉ giới thiệu với các bạn danh sách trường Nhật tốt nhất mà thôi.</p>
          </div>
        </div>

        <div class="row" style="width: 160%;">

          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon">
              <img src="http://mauweb.monamedia.net/blueuni/wp-content/uploads/2019/01/visa.png" class="attachment-medium size-medium" alt="">
            </div>
            <h4 class="title"><a href="">TỈ LỆ TỐT NGHIỆP CAO</a></h4>
            <p class="description">Nhận thức rõ rằng việc đi là cả một sự đầu tư lớn đối với phụ huynh và học sinh. Phụ huynh đã phải đầu tư nhiều chi phí và đặt bao niềm tin và hy vọng vào các bạn trẻ. Còn với các bạn, đó là bước ngoặt quan trọng của tương lai, của cuộc đời. Do vậy, chúng tôi chỉ giới thiệu với các bạn danh sách trường Nhật tốt nhất mà thôi.</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon">
              <img src="http://mauweb.monamedia.net/blueuni/wp-content/uploads/2019/01/meeting.png" class="attachment-medium size-medium" alt="">
            </div>
            <h4 class="title"><a href="">TƯ VẤN TẬN TÂM</a></h4>
            <p class="description">Nhận thức rõ rằng việc đi là cả một sự đầu tư lớn đối với phụ huynh và học sinh. Phụ huynh đã phải đầu tư nhiều chi phí và đặt bao niềm tin và hy vọng vào các bạn trẻ. Còn với các bạn, đó là bước ngoặt quan trọng của tương lai, của cuộc đời. Do vậy, chúng tôi chỉ giới thiệu với các bạn danh sách trường Nhật tốt nhất mà thôi./p>
          </div>
        </div>
      </div>
    </section><!-- #services -->
  </main>
@endsection