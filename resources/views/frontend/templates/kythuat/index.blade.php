@extends('frontend.layouts.app')
@section('content')
  <main id="main">
    <section id="about">
      <div class="container">
        <h5>BÀI VIẾT MỚI</h5>
        <ul class="list-group" style="float: left;width: 25%;font-size:15px">
          <li class="list-group-item">Du học Nhật Bản ngành xây dựng</li>
          <li class="list-group-item">Du học Nhật Bản ngành Đầu bếp</li>
          <li class="list-group-item">Du học Nhật Bản ngành cơ khí, ngành kỹ thuật ô tô</li>
          <li class="list-group-item">Du học Nhật Bản ngành du lịch nhà hàng khách sạn</li>
          <li class="list-group-item">Du học Nhật Bản ngành công nghệ thông tin năm</li>
        </ul>

        <div class="row about-cols" style="margin-left: 30%">
          <h3 class="section-header">Du học Nhật Bản ngành cơ khí, ngành kỹ thuật ô tô Nhật Bản</h3>            
          <img src="{{ asset('image/co-khi.jpg') }}" alt="" class="img-fluid">
          <!-- <div class="icon"><i class="ion-ios-speedometer-outline"></i></div> -->
          <!-- <h2 class="title"><a href="#">Danh sách câu hỏi phỏng vấn của Cục xuất nhập cảnh Nhật Bản</a></h2> -->
          <p style="font-weight: bold;font-size:17px"> 
            1. Ngành cơ khí ở Nhật Bản<br>
          </p>
          <p style="font-size:14px">
            Khi nhắc đến ngành cơ khí, có thể nghĩ ngay tới sự phát triển của đất nước Nhật Bản bởi phần lớn sức mạnh kinh tế của đất nước này nằm trong ngành công nghiệp chế tạo.<br><br>
            Từ một đất nước có vị thế sản xuất ô tô đứng hàng đầu thế giới cho tới nên kỹ thuật công nghệ sản xuất sản phẩm điện tử, linh kiện điện tử chính xác dùng trong những ngành công nghiệp kĩ thuật cao và cả các sản phẩm kim loại – hóa chất cũng luôn là một thế mạnh của Nhật Bản.<br><br>
            Chính sự phát triển dẫn đầu về ngành công nghiệp cơ khí, người Nhật luôn cố gắng duy trì và nâng cao vị thế đó bằng cách chú trọng đầu tư về giáo dục ở ngành này.<br><br>
            Với môi trường giáo dục rất tốt bằng các phương pháp giảng dạy thiên về sự phát triển kĩ năng, khơi gợi niềm đam mê và tạo điều kiện để học viên phát huy hết năng lực vốn có.<br><br>
            Bên cạnh đó, hệ thống kiến thức chuyên ngành sâu rộng gắn liền với thực tiễn kết hợp với thường xuyên tiếp xúc thực nghiệm sẽ tạo điều kiện cho các học viên nắm vững các lý thuyết được học, thực hiện được các công việc thực tế và tích lũy những kinh nghiệm quý báu cho bản thân.<br><br>
            Người Nhật Bản với sự nghiêm túc, kiên trì bền bỉ trong công việc, với đức tính kỷ luật và sự cẩn thận tỉ mỉ, làm việc đúng quy trình quy chuẩn chính là môi trường tối ưu giúp các kĩ sư hoàn thiện và phát triển các kĩ năng công việc cơ khí.<br><br>
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            2. Tại sao nên du học Nhật Bản ngành cơ khí<br>
          </p>
          <p style="font-size:14px">
            Sự phát triển thịnh vượng của nền kinh tế tại nhật bản có sự đóng góp to lớn của ngành kỹ thuật cơ khí. Từ một nước đi đầu từ ngành công nghiệp sản xuất ô tô, nước Nhật còn khẳng định vị thế kinh tế với công nghệ sản xuất linh kiện điện tử và các sản phẩm điện tử.<br><br>
            Thêm vào đó là sự đầu tư các nhà máy, phân xưởng sản xuất lớn từ các tập đoàn kinh tế, thương hiệu nổi tiếng của Nhật Bản, Hàn Quốc, Mỹ…tại Việt Nam hứa hẹn cho ngành Cơ khí chế tạo máy phát triển với tốc độ cao.<br><br>
            Kỹ sư Cơ khí có thể đảm nhận việc thiết kế, lên bản vẽ, lắp đặt hoặc gia công máy móc, thiết bị tại các nhà máy, công trình, công ty cơ khí; chuyên viên tư vấn, thiết kế, vận hành, sửa chữa máy móc, thiết bị cơ khí; lập trình gia công máy công nghệ cao hay cán bộ quản lý, điều hành kỹ thuật tại các đơn vị sản xuất, kinh doanh, dịch vụ về cơ khí phục vụ an ninh, quốc phòng, ô tô, tàu thủy, hàng không,…<br><br>
            Bởi vậy học ngành cơ khí tại Nhật Bản không chỉ khả thi vấn đề đầu ra việc làm tại Nhật mà cũng có tương lai rất sáng lạn khi quay trở về phụng sự cống hiến cho quê hương, đất nước.<br><br>
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            3. Cơ hội việc làm khi du học Nhật Bản ngành cơ khí<br><br>
          </p>
          <p style="font-size:14px">
            Tại Việt Nam, ngành cơ khí đang có tốc độ phát triển nhanh chóng và được dự báo rằng trong tương lai sẽ là một ngành công nghiệp mũi nhọn và có nhu cầu nguồn nhân lực rất lớn tạo nên nhiều cơ hội việc làm với thu nhập hấp dẫn.<br><br>
            Theo số liệu dự báo của Trung tâm Dự báo nhu cầu nhân lực và thông tin thị trường lao động Tp. HCM (Falmi) công bố thường niên thì nhu cầu nhóm ngành Cơ khí – Luyện kim – Công nghệ ô tô xe máy hiện đang đứng đầu, chiếm tỷ lệ trên 25% nhu cầu lao động.<br><br>
            Thêm vào đó là sự đầu tư các nhà máy, phân xưởng sản xuất lớn từ các tập đoàn kinh tế, thương hiệu nổi tiếng của Nhật Bản, Hàn Quốc, Mỹ…tại Việt Nam hứa hẹn cho ngành Cơ khí chế tạo máy phát triển với tốc độ cao.<br><br>
            Kỹ sư Cơ khí có thể đảm nhận việc thiết kế, lên bản vẽ, lắp đặt hoặc gia công máy móc, thiết bị tại các nhà máy, công trình, công ty cơ khí; chuyên viên tư vấn, thiết kế, vận hành, sửa chữa máy móc, thiết bị cơ khí; lập trình gia công máy công nghệ cao hay cán bộ quản lý, điều hành kỹ thuật tại các đơn vị sản xuất, kinh doanh, dịch vụ về cơ khí phục vụ an ninh, quốc phòng, ô tô, tàu thủy, hàng không,…
          </p>
          <p style="font-weight: bold;font-size:16px"> 
            3. Câu hỏi về mục đích du học Nhật Bản.<br><br>
          </p>
          <p style="font-size:14px">
            Sở dĩ Cục XNC có những câu hỏi về mục đích du học vì họ muốn kiểm tra học sinh thực chất có phải là du học hay muốn sang để đi làm kiếm tiền. Nên bạn hãy bình tĩnh trả lời để cục XNC thấy rằng mong muốn đi du học của mình là thật sự. Các câu trả lời này mình sẽ trả lời căn cứ trên lý do du học của mình nhé.<br><br>
            Các câu hỏi thường thấy<br><br>
            Tại sao bạn không học tại Việt Nam ?<br><br>
            Có rất nhiều các quốc gia trên thế giới, tại sao bạn bạn lại chọn du học Nhật ?<br><br>
            Bạn có làm thêm khi đi du học tại Nhật không ? (Hãy nói em phải cố gắng học tiếng Nhật thật tốt đã, sau đó em sẽ đi làm thêm, nhưng em chỉ đi làm để lấy kinh nghiệm, nâng cao trình độ tiếng Nhật, …)<br><br>
          </p>                    
        </div>
        <div class="form">
          <div id="sendmessage" style="font-weight:bold;">Email của bạn sẽ không được hiển thị công khai. Các trường bắt buộc được đánh dấu *</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><input type="submit"  value="Subscribe" style="background: #28a745;border: 0;width: 35%;padding: 6px 0;text-align: center;color: #fff;transition: 0.3s;cursor: pointer;"></div>
          </form>
        </div>
      </div>
    </section>
  </main>
@endsection