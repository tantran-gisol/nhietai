@extends('frontend.layouts.app')
@section('content')
  <section id="intro">
    <div class="intro-container">
      <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators"></ol>
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <div class="carousel-background">
                <img src="{{asset('image/slide-2.jpg')}}" alt="">
              </div>
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2>ĐẠI HỌC QUỐC TẾ MONA MEDIA</h2>
                  <p>Thành công của tương lai bắt đầu từ hôm nay.</p>
                  <a href="{{ url('gioithieu') }}" class="btn-get-started scrollto">GIỚI THIỆU</a>
                </div>
              </div>
            </div>

            <div class="carousel-item">
              <div class="carousel-background">
                <img src="{{asset('image/slide-1.jpg')}}" alt="">
              </div>
              <div class="carousel-container">
                <div class="carousel-content">
                  <h2>ĐẠI HỌC QUỐC TẾ MONA MEDIA</h2>
                  <p>Thành công của tương lai bắt đầu từ hôm nay.</p>
                  <a href="{{ url('lienhe') }}" class="btn-get-started scrollto">LIÊN HỆ</a>
                </div>
              </div>
            </div>
          <a class="carousel-control-prev" href="#introCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon ion-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>

          <a class="carousel-control-next" href="#introCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon ion-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>

        </div>
      </div>
    </section>

	<main id="main">

    <!--==========================
      Featured Services Section
    ============================-->
    <section id="featured-services">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 box">
            <img src="{{ asset('image/binoculars-1.png') }}" alt="" style="margin-left:43%;width:15%;">
            <h4 class="title"><a href="">Tầm nhìn</a></h4>
            <p class="description">Cầu nối giúp học sinh, sinh viên Việt Nam tiếp cận các chương trình giáo dục đào tạo nghề uy tín và chất lượng nhất của các nền giáo dục lớn trên thế giới. Vươn lên trở thành trường đại học hàng đầu Việt Nam</p>
          </div>

          <div class="col-lg-4 box box-bg">
            <img src="{{ asset('image/diamond-1.png') }}" alt="" style="margin-left: 43%;width: 15%">
            <h4 class="title"><a href="">Sứ mệnh</a></h4>
            <p class="description">Đào tạo ra nguồn nhân lực chất lượng và hiệu quả, chi phí du học hợp lý, tỉ lệ đỗ visa du học cao, giúp học sinh, sinh viên yên tâm hoàn thành mục tiêu đặt ra trong thời gian ngắn nhất.</p>
          </div>

          <div class="col-lg-4 box">
            <img src="{{ asset('image/cubes-stack.png') }}" alt="" style="margin-left: 43%;width: 15%">
            <h4 class="title"><a href="">Phương châm</a></h4>
            <p class="description">Định hướng tương lai giúp học sinh, sinh viên có thể học tập, quyết định ngành học cũng như lựa chọn công việc phù hợp sở thích, tiềm năng của bản thân trong quá trình du học tại Nhật Bản.</p>
          </div>

        </div>
      </div>
    </section><!-- #featured-services -->
        <!--==========================
      Clients Section
    ============================-->
    <section id="clients" class="wow fadeInUp">
      <div class="container">
        <h3 class="section-header">CHUYÊN NGÀNH ĐÀO TẠO</h3>
        <div class="owl-carousel clients-carousel">
          <img src="{{ asset('image/co-khi.jpg') }}" alt="">
          <img src="{{ asset('image/sushi.jpg') }}" alt="">
          <img src="{{ asset('image/xay-dung.jpg') }}" alt="">
          <img src="{{ asset('image/kinh-doanh.jpg') }}" alt=""> 
          <img src="{{ asset('image/ho-ly-san.jpg') }}" alt="">
          <img src="{{ asset('image/cntt-jpg') }}" alt="">
          <img src="{{ asset('image/nh-ks.jpg') }}" alt="">
          <img src="{{ asset('image/ke-toan.jpg') }}" alt="">
        </div>

      </div>
    </section><!-- #clients -->

    <!--==========================
      Clients Section
    ============================-->


    <div class="section-content relative">
      <div class="gap-element" style="display:block; height:auto; padding-top:60px" class="clearfix"></div>
      <div class="row"  id="row-1042191683">
        <div class="col small-12 large-12"  >
          <div class="col-inner text-center"  >
            <img src="{{ asset('image/Banner.jpg') }}">
          </div>
        </div>
      </div>
    </div>

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container">
        <h3 class="section-header">SINH VIÊN SAU TỐT NGHIỆP</h3>
        <div class="row">
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon">
              <img src="{{ asset('image/icon1.png') }}">
            </div>
            <h4 class="title"><a href="">Chuyên môn giỏi</a></h4>
            <p class="description">Lý thuyết vững vàng, thực hành thành thạo; 100% sinh viên tốt nghiệp Mona có việc làm trong năm đầu tiên.</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon">
              <img src="{{ asset('image/icon2.png') }}">
            </div>
            <h4 class="title"><a href="">Kỹ năng đa dạng</a></h4>
            <p class="description">Phát triển tối đa năng lực bản thân với các kỹ năng mềm cần thiết: tiếng Anh (IELTS 5.0), Tin học (MOS 750), Sinh tồn (bơi 50m), …</p>
          </div>
          <div class="col-lg-4 col-md-6 box wow bounceInUp" data-wow-duration="1.4s">
            <div class="icon">
              <img src="{{ asset('image/icon3.png') }}">
            </div>
            <h4 class="title"><a href="">Đạo đức chuẩn mực</a></h4>
            <p class="description">Rèn luyện trong môi trường nghiêm túc: Hiếu thảo với cha mẹ, Thượng tôn pháp luật, Tinh thần phụng sự cộng đồng.</p>
          </div>

        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action" class="wow fadeIn" style="height: 440px;">
      <div class="container text-center">
        <h3>GÓC CHIA SẺ</h3>
        <p>Trước khi tìm đến du học Mona Media, tôi cũng tham khảo và đã đi qua rất nhiều đơn vị tư vấn du học khác nhau. Nhưng phần lớn họ đều có chi phí khá cao, một số đơn vị có giá thành thấp nhưng dịch vụ rất kém chất lượng. Riêng ở đây thì tôi nhận thấy đươc giá trị giữa dịch vụ mang lại và chi phí bỏ ra là tương đối hợp lý cũng như những thắc mắc về vấn đề du học Nhật Bản của tôi được giải đáp rất cụ thể. Đó là lý do mà tôi chọn du học Mona Media để làm nơi tư vấn du học cho con tôi. Chúc công ty ngày một phát triển.</p>
        <!-- <a class="cta-btn" href="#">Nancy</a> -->
      </div>
    </section><!-- #call-to-action -->
    <section id="services">
      <div class="container">
        <h3 class="section-header">TIN TỨC</h3>
        <div class="new-content row">
          @if((new \Jenssegers\Agent\Agent())->isDesktop())
            @php
              $posts = $postsPC;
            @endphp
          @else
            @php
              $posts = $postsSP;
            @endphp
          @endif
          @if(count($posts))
            @foreach($posts as $post)
              <div class="card-content col-lg-4 col-6">
                <div class="card-new-content">
                  <a href="{{ route('frontend.post.show', $post->slug) }}">
                    <div class="card-new-content-img img-wrap" style="background: url('{{ ($post->featureImage) ? Helper::getMediaUrl($post->featureImage, 'medium') : Helper::getDefaultCover($post) }}');">
                    @if($post->featureImage)
                      <img src="{{ Helper::getMediaUrl($post->featureImage, 'medium') }}" />
                    @else
                      <img src="{{ Helper::getDefaultCover($post) }}">
                    @endif
                  </div>
                </a>

                  <div class="post-man d-flex justify-content-between">
                    <label class="posted-date d-none d-md-block">{{ date('Y-m-d', strtotime($post->created_at)) }}</label>
                    <label class="card-category"><a href="{{ route('frontend.user.show', $post->user) }}">{{ $post->user->name }}</a></label>
                  </div>
                  <div class="artical-title">
                    <a href="{{ route('frontend.post.show', $post->slug) }}">{{ $post->title }}</a>
                  </div>
                </div>
              </div>
            @endforeach
          @endif
        </div>
        <div class="row">
          <!-- pagination -->
          {{ $posts->appends(request()->query())->links('frontend.parts.pagination') }}
          <!--x-- pagination -->
        <!-- </div> -->

        <!---x--- card new content ---x--->

      </div>
     </div> 
    </section>
  </main>
@endsection