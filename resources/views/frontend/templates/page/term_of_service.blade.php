@extends('frontend.layouts.app')
@section('content')
	<section class="main">

		<!-- main term of service page content -->

		<div class="main-term-of-service-page">
			<div class="container">
				<div class="term-of-service-page-header row">
					<div class="col-12">
						<h1>管理者設定</h1>
					</div>
				</div>
			
				<div class="line"></div>

				<div class="term-of-service-page row">
					
					<!-- left terms of service content -->

					<div class="term-of-service-content col-lg-8 col-md-8 col-12">
						<div class="term-of-service">
							<h3>第1条  総則</h3>

							<p>会社（以下、「当社」といいます。）は、https：// adivADIVA株式a-world.jp/（以下、といいます。）を運営しています <br></p>

							<p>「本サイト」本サイトにおいては、当社、その関連媒体及び提携先（以下「当社ら」といいます。）により、本サイトに関連するサービス、コンテンツ又はアプリケーション（以下、本サイトとあわせて、「当社サービス」といいます。）が提供されています</p>

							<p>これらの当社サービスを利用される方（以下「ユーザー」といいます。）は、こちらの利用規約（以下https://adiva-world.jp/policyの当社のプライバシーポリシーとあわせて、「本利用規約」といいます。）を慎重にお読みいただき、ご理解頂いたうえでご利用ください</p>

							<p>本利用規約には、本サイト及び当社サービスの利用に関する諸条件が明記されており、登録の有無を問わず、コンテンツ、情報及びその他の素材・サービスの投稿者のみならず、全てのユーザーに適用されます</p>
						</div>

						<div class="term-of-service">
							<h3>第2条  利用資格</h3>

							<p>当社サービスのコューザーは、満13歳以上でなければなりません。ユーザーは13歳以上であること（未成年者の場合は、加えて、当社サービスの利用について親権者などの法定代理人の同意があること）を表明し、保証しなければなりません</p>

							<p>13歳未満の場合又は未成年者であるにもかかわらず、親権者などの法定代理人の同意を得ていない場合当社は当社サイトの利用をお断りすることができます。</p>

							<p>当社は、当社の裁量で、いかなる者に対して当社サービスを提供するか否かを判断し、いつでも資格要件を変更することができるものとします。</p>

							<p>ユーザーは、すべベての法令その他の規則を遵守しなければならず、本利用規約ら又は法令おそれがある場合には、直ちに当社サービスへのアクセスを中止しなければなりません。などに反する</p>
						</div>
					</div>

					<!--x-- left terms of service content --x-->

					<!-- right category content -->

					@include('frontend.parts.right_sidebar')

					<!--x-- right category content --x-->

				</div>
			</div>
		</div>

		<!--x-- main term of service page content --x-->

	</section>
@endsection