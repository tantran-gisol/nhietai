@extends('frontend.layouts.app')
@section('content')
<div class="idance" style="margin-top:11%">
    <div class="schedule content-block">
        <div class="container">
            <h3 data-aos="zoom-in-up" class="aos-init aos-animate">LỊCH HỌC</h3>
        
            <div class="timetable" style="margin-bottom:5%">
        
              <!-- Schedule Top Navigation -->
              <nav class="nav nav-tabs">
                <a class="nav-link active">Thứ 2</a>
                <a class="nav-link active">Thứ 3</a>
                <a class="nav-link active">Thứ 4</a>
                <a class="nav-link active">Thứ 5</a>
                <a class="nav-link active">Thứ 6</a>
                <a class="nav-link active">Thứ 7</a>
                <a class="nav-link active">Chủ nhật</a>
              </nav>
        
              <div class="tab-content">
                <div class="tab-pane show active">
                  <div class="row">
        
                    <!-- Schedule Item 1 -->
                    <div class="col-md-6">
                      <div class="timetable-item">
                        <div class="timetable-item-img">
                          <img src="https://via.placeholder.com/100x80/FFB6C1/000000" alt="Contemporary Dance">
                        </div>
                        <div class="timetable-item-main">
                          <div class="timetable-item-time">4:00pm - 5:00pm</div>
                          <div class="timetable-item-name">Tiếng Trung</div>
                          <a href="#" class="btn btn-primary btn-book">Book</a>
                          <div class="timetable-item-like">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <div class="timetable-item-like-count">11</div>
                          </div>
                        </div>
                      </div>
                    </div>
        
                    <!-- Schedule Item 2 -->
                    <div class="col-md-6">
                      <div class="timetable-item">
                        <div class="timetable-item-img">
                          <img src="https://via.placeholder.com/100x80/00FFFF/000000" alt="Break Dance">
                        </div>
                        <div class="timetable-item-main">
                          <div class="timetable-item-time">5:00pm - 6:00pm</div>
                          <div class="timetable-item-name">Tiếng Lào</div>
                          <a href="#" class="btn btn-primary btn-book">Book</a>
                          <div class="timetable-item-like">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <div class="timetable-item-like-count">28</div>
                          </div>
                        </div>
                      </div>
                    </div>
        
                    <!-- Schedule Item 3 -->
                    <div class="col-md-6">
                      <div class="timetable-item">
                        <div class="timetable-item-img">
                          <img src="https://via.placeholder.com/100x80/8A2BE2/000000" alt="Street Dance">
                        </div>
                        <div class="timetable-item-main">
                          <div class="timetable-item-time">5:00pm - 6:00pm</div>
                          <div class="timetable-item-name">Tiếng Campuchia</div>
                          <a href="#" class="btn btn-primary btn-book">Book</a>
                          <div class="timetable-item-like">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <div class="timetable-item-like-count">28</div>
                          </div>
                        </div>
                      </div>
                    </div>
        
                    <!-- Schedule Item 4 -->
                    <div class="col-md-6">
                      <div class="timetable-item">
                        <div class="timetable-item-img">
                          <img src="https://via.placeholder.com/100x80/6495ED/000000" alt="Yoga">
                        </div>
                        <div class="timetable-item-main">
                          <div class="timetable-item-time">7:00pm - 8:00pm</div>
                          <div class="timetable-item-name">Tiếng Việt</div>
                          <a href="#" class="btn btn-primary btn-book">Book</a>
                          <div class="timetable-item-like">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <div class="timetable-item-like-count">23</div>
                          </div>
                        </div>
                      </div>
                    </div>
        
                    <!-- Schedule Item 5 -->
                    <div class="col-md-6">
                      <div class="timetable-item">
                        <div class="timetable-item-img">
                          <img src="https://via.placeholder.com/100x80/00FFFF/000000" alt="Stretching">
                        </div>
                        <div class="timetable-item-main">
                          <div class="timetable-item-time">6:00pm - 7:00pm</div>
                          <div class="timetable-item-name">Tiếng Đông Lào</div>
                          <a href="#" class="btn btn-primary btn-book">Book</a>
                          <div class="timetable-item-like">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <div class="timetable-item-like-count">14</div>
                          </div>
                        </div>
                      </div>
                    </div>
        
                    <!-- Schedule Item 6 -->
                    <div class="col-md-6">
                      <div class="timetable-item">
                        <div class="timetable-item-img">
                          <img src="https://via.placeholder.com/100x80/008B8B/000000" alt="Street Dance">
                        </div>
                        <div class="timetable-item-main">
                          <div class="timetable-item-time">8:00pm - 9:00pm</div>
                          <div class="timetable-item-name">Tiếng Thái</div>
                          <a href="#" class="btn btn-primary btn-book">Book</a>
                          <div class="timetable-item-like">
                            <i class="fa fa-heart-o" aria-hidden="true"></i>
                            <i class="fa fa-heart" aria-hidden="true"></i>
                            <div class="timetable-item-like-count">9</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
  </div>
@endsection