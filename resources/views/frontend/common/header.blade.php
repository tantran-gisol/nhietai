<header id="header">
    <div class="container-fluid">

      <div id="logo" class="pull-left">
        <!-- <h1><a href="#intro" class="scrollto">TPP-FIT</a></h1> -->
        <a href="{{ url('/') }}" ><img src="{{ asset('image/logo.png') }}"style="width: 150px;"></a>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title="" /></a>-->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li><a href="{{ url('gioithieu') }}">GIỚI THIỆU</a></li>
          <li class="menu-has-children"><a href="">HỘI THẢO</a>
            <ul>
              <li><a href="{{ url('danhsach') }}">DANH SÁCH TRƯỜNG NHẬT NGỮ</a></li>
              <li><a href="{{ url('tuyensinh') }}">TUYỂN SINH DU HỌC NHẬT BẢN</a></li>
            </ul>
          </li> 
          <li class="menu-has-children"><a href="">THÔNG TIN NGÀNH HỌC</a>
            <ul>
              <li><a href="{{ url('dieuduong') }}">ĐIỀU DƯỠNG</a></li>
              <li><a href="{{ url('cntt') }}">CÔNG NGHỆ THÔNG TIN</a></li>
              <li><a href="{{ url('nhks') }}">NHÀ HÀNG - KHÁCH SẠN</a></li>
              <li><a href="{{ url('kythuat') }}">CƠ KHÍ - KỸ THUẬT Ô TÔ</a></li>
              <li><a href="{{ url('daubep') }}">ĐẦU BẾP</a></li>
              <li><a href="{{ url('xaydung') }}">XÂY DỰNG</a></li>
            </ul>
          </li>          
          <!-- <li><a href="{{ url('tintuc') }}">TIN TỨC</a></li> -->
          <li><a href="{{ url('lichhoc') }}">LỊCH HỌC</a></li>
          <li><a href="{{ url('lienhe') }}">LIÊN HỆ</a></li>
          <li class="menu-has-children"><a href="{{ url('dangnhap') }}">ĐĂNG NHẬP</a>
            <ul>
              <li><a href="">ĐĂNG KÝ</a></li>
              <li><a href="">THOÁT</a></li>
            </ul>
          </li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header>