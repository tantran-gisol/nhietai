@extends('admin.layouts.app')

@section('content')
	<!---------- admin-edit main section ---------->
	<div class="container" style="max-width:665px;">
		<div class="avatar text-center">
			<a href="{{ route('admin.user.edit', Auth::user()->id) }}" style="color: #212529;">
				<div class="avatar-img">
					<img src="{{ !empty(Auth::user()->avatar) ? Helper::getImageUrl(Auth::user()->avatar) : asset('image/no-img.png') }}">
				</div>
				<div class="avatar-txt">
					{{Auth::user()->email}}<br>
					Sửa thông tin của tôi
				</div>
			</a>
		</div>

		<div class="setting-choice row text-center">
			<div class="col-lg-3 col-6 mr-auto ">
				<a href="{{ route('admin.post.create') }}"><!-- <i class="fas fa-pencil-alt"></i> --><img src="{{ asset('image/icon/ic-pencil.png') }}">
				<p>Bài đăng</p></a>
			</div>
			@can('indexBySite',App\Models\Post::class)
				<div class="col-lg-3 col-6 mr-auto ">
					<a href="{{ route('admin.post.indexBySite') }}"><!-- <i class="far fa-copy"></i> --><img src="{{ asset('image/icon/ic-content.png') }}">
					<p>Danh sách nội dung</p></a>
				</div>
			@endcan
			@can('edit', Auth::user()->site)
				<div class="col-lg-3 col-6 mr-auto ">
					<a href="{{ route('admin.site.edit', Auth::user()->site_id) }}"><!-- <i class="fas fa-cogs"></i> --><img src="{{ asset('image/icon/ic-cogs.png') }}">
					<p>Cài đặt Trang web</p></a>
				</div>
			@endcan
			@can('index',App\Models\User::class)
				<div class="col-lg-3 col-6 mr-auto ">
					<a href="{{ route('admin.user.index') }}"><!-- <i class="fas fa-user"></i> --><img src="{{ asset('image/icon/ic-account.png') }}">
					<p>Cài đặt quản trị viên</p></a>
				</div>
			@endcan
			@can('index',App\Models\Tag::class)
				<div class="col-lg-3 col-6 mr-auto ">
					<a href="{{ route('admin.tag.index') }}"><!-- <i class="fas fa-tag"></i> --><img src="{{ asset('image/icon/ic-tag.png') }}">
					<p>Quản lý thẻ</p></a>
				</div>
			@endcan
			<div class="col-lg-3 col-6 mr-auto ">
				<a href="{{ route('admin.google_statistic.index') }}"><!-- <i class="fas fa-chart-bar"></i> --><img src="{{ asset('image/icon/ic-google.png') }}">
				<p>Phân tích truy cập</p></a>
			</div>
			@can('index',App\Models\Media::class)
				<div class="col-lg-3 col-6 mr-auto ">
					<a href="{{ route('admin.media.index') }}"><!-- <i class="far fa-image"></i> --><img src="{{ asset('image/icon/ic-img.png') }}">
					<p>Quản lý nội dung</p></a>
				</div>
			@endcan
			<div class="col-lg-3 col-6 mr-auto ">
				<a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><!-- <i class="fas fa-sign-out-alt"></i> --><img src="{{ asset('image/icon/ic-signout.png') }}">
				<p>Đăng xuất</p></a>
			</div>
			<div class="col-lg-3 col-6 mr-auto ">
				<a href="{{ route('frontend.home.index') }}" target="_blank"><!-- <i class="fas fa-arrow-left"></i> --><img src="{{ asset('image/icon/ic-left-arrow.png') }}">
				<p>Quay lại</p></a>
			</div>
		</div>
	</div>
	<!-----x----- admin-edit main section ---test push -----x----->
@endsection