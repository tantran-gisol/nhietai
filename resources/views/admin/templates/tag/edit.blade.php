@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main" >
		<div class="post-edit" style="padding: 25px;">
			<h3>Quản lý thẻ</h3>
      <h6 style="color: black; font-size: 14px;">Quản lý các thẻ được sử dụng làm ứng viên đầu vào khi tạo bài viết.</h6>
      <h6 style="color: black; font-size: 14px;">Bằng cách đăng ký trước các thẻ, bạn có thể sử dụng chúng như các ứng cử viên nhập thẻ khi tạo bài viết.</h6>

      @include('admin.parts.alert')

    </div>
    <div class="tag-edit">
			<form action="{{ route('admin.tag.update', $tag) }}" method="POST">
				<div class="form-group row" style="padding: 1rem 0 2rem 0;">
					<div class="col-lg-2 col-md-3 text-left text-md-right">
						<label>Tên</label>
					</div>
					<div class="col-12 col-lg-10 col-md-9">
						<input type="text" class="form-control" name="name" value="{{ old('name', $tag->name) }}" required="" />
					</div>
				</div>
				<div class="form-group row" style="padding: 1rem 0 2rem 0;">
					<div class="col-lg-2 col-md-3 text-left text-md-right">
						<label>Biểu tượng</label>
					</div>
					<div class="col-12 col-lg-10 col-md-9">
						<div style="opacity: 0.5;">
							<p>Hình ảnh này sẽ được hiển thị dưới dạng một biểu tượng trên trang danh sách danh mục.</p>
						</div>
						@if($tag->featured_image)
							<div class="mb-3" style="width: 240px;">
								<img src="{{ $tag->featured_image }}" data-image-input-preview="featured_image">
							</div>
						@endif
						
						<input type="text" class="form-control" placeholder="" name="featured_image" value="{{ $tag->featured_image }}" style="margin-bottom: .5rem;">

						<button type="button" class="btn btn-no-radius open-library" data-input-name="featured_image" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>Chọn từ thư viện</button>
					</div>
				</div>
				<div class="form-group row" style="padding: 1rem 0 2rem 0;">
					<div class="col-lg-2 col-md-3 text-left text-md-right">
						<label>Hình ảnh hiển thị trong thanh bên</label>
					</div>
					<div class="col-12 col-lg-10 col-md-9">
						<div style="opacity: 0.5;">
							<p>Hình ảnh này sẽ xuất hiện trong danh sách ở thanh bên</p>
						</div>
						@if($tag->sub_featured_image)
							<div class="mb-3" style="width: 240px;">
								<img src="{{ $tag->sub_featured_image }}" data-image-input-preview="sub_featured_image">
							</div>
						@endif
						
						<input type="text" class="form-control" placeholder="" name="sub_featured_image" value="{{ $tag->sub_featured_image }}" style="margin-bottom: .5rem;">

						<button type="button" class="btn btn-no-radius open-library" data-input-name="sub_featured_image" data-input-type="url" data-toggle="modal" data-target="#library-modal" style="background: #111; color: #fff;"><i class="far fa-image" style="color: #fff;"></i>Chọn từ thư viện</button>
					</div>
				</div>
				<button class="btn btn-no-radius" style="background: #111; color: #fff;">Cập nhật</button>
				{{ csrf_field() }}
			</form>
		</div>
		
	</section>
	<!---------- admin-edit main section ---------->
	<!-- Library modal -->
	@include('admin.templates.media.parts.modal-library')
	<!--x-- Library modal -->
@endsection