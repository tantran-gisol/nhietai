<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuTag extends Model
{
	protected $fillable = [
		'tag_id', 'order', 'shown_in_header_menu', 'shown_in_feed_label', 'display_text_enabled', 'display_detail', 'shown_in_editor', 'reverse_order', 'hide_from_display'
	];
	public function tag()
  {
    return $this->belongsTo(Tag::class);
  }
}