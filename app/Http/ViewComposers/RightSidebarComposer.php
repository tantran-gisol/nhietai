<?php

namespace App\Http\ViewComposers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Setting;
use Illuminate\View\View;
use Helper;

class RightSidebarComposer
{
    /**
     * The user repository implementation.
     *
     */
    protected $categoryModel;
    protected $postModel;
    protected $settingModel;

    /**
     * Create a new profile composer.
     *
     * @param MenuService $menuService
     */
    public function __construct(Category $categoryModel, Post $postModel, Setting $settingModel)
    {
        // Dependencies automatically resolved by service container...
        $this->categoryModel = $categoryModel;
        $this->postModel = $postModel;
        $this->settingModel = $settingModel;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $featureCatId = $this->settingModel->getSettingData('siderbar_feature_cat_id');
        $featureNumber = $this->settingModel->getSettingData('siderbar_feature_number');
        $seriesCatId = $this->settingModel->getSettingData('siderbar_series_cat_id');
        $seriesNumber = $this->settingModel->getSettingData('siderbar_series_number');
        $featureCategories = $this->categoryModel->getFeatureCategories($featureCatId, $featureNumber);
        $seriesCategories = $this->categoryModel->getSeriesCategories($seriesCatId, $seriesNumber);
        $rankingNumber = $this->settingModel->getSettingData('siderbar_ranking_number');
        $topPosts = $this->postModel->getTopPosts($rankingNumber);
        $instaFeeds = Helper::get_instagram_feed(4);
        $view->with([
            'featureCategories' => $featureCategories,
            'seriesCategories' => $seriesCategories,
            'topPosts' => $topPosts,
            'instaFeeds' => $instaFeeds
        ]);
    }

}