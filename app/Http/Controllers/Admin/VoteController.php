<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Vote;
use Auth;

class VoteController extends Controller
{
    protected $voteModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Vote $voteModel)
    {
        $this->voteModel = $voteModel;
    }

    public function create(Request $request)
    {
        $this->authorize('create', Vote::class);
        return view('admin.templates.vote.create');
    }

    public function store(Request $request)
    {
        $this->authorize('store', Vote::class);
        $this->validate($request, [
          'name' => 'required',
          'content' => 'required',
        ],[],[
            'name' =>  trans('table_fields.votes.name'),
            'content' =>  trans('table_fields.votes.content')
        ]);
        $voteData = $request->except(['_token']);
        $vote = $this->voteModel->create($voteData);
        return redirect()->route('admin.setting.vote')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.vote')]));
    }

    public function edit(Request $request, $id)
    {
        $vote = $this->voteModel->findOrFail($id);
        $this->authorize('edit', $vote);
        return view('admin.templates.vote.edit')->with([
            'vote' => $vote
        ]);
    }

    public function update(Request $request, $id)
    {
        $vote = $this->voteModel->findOrFail($id);
        $this->authorize('update', $vote);
        $this->validate($request, [
          'name' => 'required',
          'content' => 'required',
        ],[],[
            'name' =>  trans('table_fields.votes.name'),
            'content' =>  trans('table_fields.votes.content')
        ]);
        $voteData = $request->except(['_token']);
        $vote->fill($voteData)->save();
        return redirect()->route('admin.vote.edit', $vote)->with('success', trans('messages.success.update', ['Module' => trans('module.entry.vote')]));
    }

    public function destroy($id)
    {
        $vote = $this->voteModel->findOrFail($id);
        $this->authorize('destroy', $vote);
        $this->voteModel->destroy($id);
        return redirect()->route('admin.setting.vote')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.vote')]));
    }

}
