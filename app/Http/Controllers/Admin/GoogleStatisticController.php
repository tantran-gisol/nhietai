<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Analytics;
use Spatie\Analytics\Period;
use Carbon\Carbon;

class GoogleStatisticController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('period') && is_numeric($request->period)){
            $period = Period::days($request->period);
        }else{
            $period = Period::days(7);
        }
        if($request->has('metric') && !empty($request->metric)){
            $metric = $request->metric;
        }else{
            $metric = 'sessions';
        }
        $responseChartData = Analytics::performQuery(
            $period,
            'ga:'.$metric,
            ['dimensions' => 'ga:date']
        );
        $chartData = collect($responseChartData['rows'] ?? [])->map(function (array $dateRow) {
            return [
                'date' => Carbon::createFromFormat('Ymd', $dateRow[0])->toDateString(),
                'count' => (int) $dateRow[1],
            ];
        })->toArray();

        //
        $dataTotals = [
            'visitors' => $this->getTotalByMetric($period, 'users'),
            'visits' => $this->getTotalByMetric($period, 'sessions'),
            'pageViews' => $this->getTotalByMetric($period, 'pageviews'),
            'bounceRate' => round($this->getTotalByMetric($period, 'bounceRate'), 2),
            'avgSessionDuration' => $this->getTotalByMetric($period, 'avgSessionDuration'),
            'avgTimeOnPage' => $this->getTotalByMetric($period, 'avgTimeOnPage'),
            'pageLoadTime' => $this->getTotalByMetric($period, 'pageLoadTime'),
            'organicSearches' => $this->getTotalByMetric($period, 'organicSearches'),
        ];
        return view('admin.templates.statistic.google')->with(['chartData' => $chartData, 'dataTotals' => $dataTotals]);
    }

    public function getTotalByMetric($period, $metric) {
        $response = Analytics::performQuery(
            $period,
            'ga:'. $metric
        );
        if($response['rows'] !== null){
            return array_sum(array_filter(array_column($response['rows'], 0)));
        }else{
            return 0;
        }
    }
    
}
